function metodos_diferencias(varargin)
%METODOS_DIFERENCIAS Aproxima uno o varios valores de X.
%
%   METODOS_DIFERENCIAS({X0, X1, ..., Xn}, {Y0, Y1, ..., Yn}, ...
%                       {V0, V1, ..., Vn}, PR, F)
%
%   Utiliza los valores de {X0, X1, ..., Xn} correspondientes a su valor 
%   respectivo de {Y0, Y1, ..., Yn} para encontrar una aproximacion de el o
%   los valores {V0, V1, ..., Vn}. Si se proporciona la funci�n F entonces 
%   verifica el error.
%
%   METODOS_DIFERENCIAS({X0, X1, ..., Xn}, {V0, V1, ..., Vn}, PR, F)
%
%   Utiliza los valores de {X0, X1, ..., Xn} y los evalua en la funci�n F
%   para aproximar el o los valores {V0, V1, ..., Vn}. Adem�s comprueba el 
%   error.
%
%   El resultado se muestra con la precisi�n especificada PR.

fprintf('\n# M�todo de diferencias divididas\n\n');

% -------------------------------------------------------------------------
% PREPARACI�N
% -------------------------------------------------------------------------

% chequeo de errores
if ~iscell(varargin{1})
    error('El primer argumento debe de ser un arreglo de celdas.');
end

if ~iscell(varargin{2})
    error('El segundo argumento debe de ser un arreglo de celdas.');
end

% se ha utilizado la primera forma de llamarlo
if iscell(varargin{3})
        
    XS = varargin{1};
    YS = varargin{2};
    X  = varargin{3};
    PR = varargin{4};
    
    if length(varargin) > 4   
        FU = varargin{5};
    else
        FU = [];
    end
   
% se ha utilizado la segunda forma    
else
    
    XS = varargin{1};
    X  = varargin{2};
    PR = varargin{3};
    FU = varargin{4};
    YS = num2cell(subs(FU, cell2mat(XS)));  
    
end

% los datos deben de ser coherentes
if length(XS) ~= length(YS)
    error('No se han proporcionado la misma cantidad de valores de X y Y');
end

fprintf('## Preparaci�n:\n\n');

for i = 1:length(XS)
    fprintf(['  f(%.3f) = %.', num2str(PR), 'f\n'], ...
             XS{i}, YS{i});
end

fprintf('\n');

% -------------------------------------------------------------------------
% ALGORITMO
% -------------------------------------------------------------------------

fprintf('## Procedimiento:\n\n');

% creamos el arreglo de celda vacio F
F = num2cell(zeros(length(XS)));

% ponemos los valores de la primera columna
F(:, 1) = YS;

% creamos las cadenas de las variables F para evaluarlas despues
varF = {};

for j = 0:(length(XS)-1)
    for i = 0:(length(XS)-1)
        varF{i + 1, j + 1} = sprintf('F%d%d', i, j);        
    end    
end

% ahora la de las variables X
varX = {};

for j = 0:(length(XS)-1)
    varX{j + 1} = sprintf('X%d', j);
end

% en la variable vamos a ir guardando la expresi�n (x - x0)(x - x1)...
% (x - xn) y tambi�n guardamos el polinomio final
multiplic = '';
polinomio = sprintf(['+(%.', num2str(PR), 'f)'], F{1, 1});

% iteramos para crear una matriz tiangular inferior
for j = 1:(length(XS)-1)  
    for i = j:(length(XS)-1)
       
        expresion = sprintf('(F%d%d - F%d%d)/(X%d - X%d)', i, j-1, i-1, j-1, ...
                            i, i-j);
                                
        % primero evaluamos con las F
        evaluada = subs(expresion, varF(:), F(:));
        
        % despues con los x0, x1, ..., xn
        evaluada = subs(evaluada, varX(:), XS(:));
                    
        % ultimamente con la x que deseamos aproximar
        F{i + 1, j + 1} = subs(evaluada, X);
        
        % mostramos la igualdad para copiarla al parcial :)
        fprintf(['-------------------------------------------------------', ...
                 '-------------------------\n']);     
        fprintf('F%d%d:', i, j);
        valor = sprintf(['%.', num2str(PR), 'f'], F{i + 1, j + 1});        
        pretty(sym([expresion, '=', valor]))
        
        % si se trata del primer valor en la columna entonces expandemos el
        % polinomio final                
        if j == i

            % le agregamos el coeficiente            
            polinomio = [polinomio, ...
                         sprintf(['+(%.', num2str(PR), 'f)'], F{i + 1, j + 1})];                     
                     
            % agregamos un factor mas a la multiplicaci�n
            multiplic = [multiplic, '*(X - (', num2str(XS{j}), '))'];
                     
            % agregamos las multiplicaiones
            polinomio = [polinomio, multiplic];

        end             
    end
end

fprintf(['-------------------------------------------------------', ...
         '-------------------------\n\n']);  
fprintf('## Tabla:\n');              

% mostramos la tabla
for j = 1:length(XS)
    
    % el valor de la X correspondiente a la fila
    fprintf('\n  %.3f\t', XS{j});
    
    for i = 1:j        
        % imprimimos renglon por renglon los datos
        fprintf(['%.', num2str(PR), 'f\t'], F{j, i});        
    end
    
end

fprintf('\n\n');

% -------------------------------------------------------------------------
% RESULTADOS
% -------------------------------------------------------------------------

fprintf('## Polinomio sin formato:\n\n');
fprintf(['  ', polinomio, '\n\n']);
fprintf('## Polinomio con formato y simplificado:\n');
pretty(sym(polinomio));
fprintf('\n');

% evaluamos las X que se deseen en el vector junto con el valor exacto (si
% se puede) y su error
for i = 1:length(X)
    fprintf('## Resultado para X = %.3f:\n\n', X{i});
    fprintf(['  Aproximado: P(%.3f) = %.', num2str(PR), 'f\n'], X{i}, ...
            subs(polinomio, X{i}));
        
    % si se tiene la funcion se calcula el valor exacto y el error
    if FU
        fprintf(['      Exacto: F(%.3f) = %.', num2str(PR), 'f\n'], X{i}, ...
            subs(FU, X{i}));
        fprintf('       Error: %.2E\n', ...
            abs(subs(FU, X{i}) - subs(polinomio, X{i})));
    end
    
    fprintf('\n');    
end
