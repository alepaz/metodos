function P = metodos_algoritmo_neville(XS, YS, X, PR)
%METODOS_ALGORITMO_NEVILLE Funci�n utilizada por METODOS_NEVILLE

% creamos el arreglo de celda vacio Q
Q = num2cell(zeros(length(XS)));

% ponemos los valores de la primera columna
Q(:, 1) = YS;

% creamos las cadenas de las variables Q para evaluarlas despues
varQ = {};

for j = 0:(length(XS)-1)
    for i = 0:(length(XS)-1)
        varQ{i + 1, j + 1} = sprintf('Q%d%d', i, j);        
    end    
end

% ahora la de las variables X
varX = {};

for j = 0:(length(XS)-1)
    varX{j + 1} = sprintf('X%d', j);
end

% iteramos a traves de la matriz Q para crearla
for j = 1:(length(XS)-1)  
    for i = j:(length(XS)-1)
       
        expresion = ...
            sprintf('((X - X%d)*Q%d%d - (X - X%d)*Q%d%d)/(X%d - X%d)', ...
                    i-j, i, j-1, i, i-1, j-1, i, i-j);
                
        % primero evaluamos con las Q
        evaluada = subs(expresion, varQ(:), Q(:));
        
        % despues con los x0, x1, ..., xn
        evaluada = subs(evaluada, varX(:), XS(:));
                    
        % ultimamente con la x que deseamos aproximar
        Q{i + 1, j + 1} = subs(evaluada, X);
                
        % mostramos la igualdad para copiarla al parcial :)
        fprintf(['-------------------------------------------------------', ...
                 '-------------------------\n']);        
        fprintf('Q%d%d:', i, j);
        valor = sprintf(['%.', num2str(PR), 'f'], Q{i + 1, j + 1});        
        pretty(sym([expresion, '=', valor]))              
        
    end
end

fprintf(['-------------------------------------------------------', ...
         '-------------------------\n']);  

% el resultado
P = Q{end};