
disp('Metodo de Newton-Raphson')
P0 = input('Inserte el punto inicial P0: ');
syms x
g = input('Introduzca la funcion: ');
error = input('Introduzca el error: ');
gp = diff(g,x);
ge = subs(g,P0);
gpe = subs(gp,P0);
P = P0 - (ge/gpe);
E=abs(P0-P);
cont=1;
fprintf('n=%3.0f P0=%.15f P=%.15f E=%.15e\n', cont,P0,P,E);
while E>error
    cont=cont+1;
    P0=P;
    ge = subs(g,P0);
    gpe = subs(gp,P0);
    P = P0 - (ge/gpe);
    E=abs(P0-P);
    fprintf('n=%3.0f P0=%.15f P=%.15f E=%.15e\n', cont,P0,P,E);
end
fprintf('El valor aproximado de P es:  %.15f\n',P)