clear all
clc
disp('Metodo de punto fijo')
P0 = input('Inserte el punto inicial P0: ');
syms x
g = input('Introduzca la funcion: ');
error = input('Introduzca el error: ');
P = subs(g,P0);
E=abs(P-P0);
cont=1;
while E>error
    cont=cont+1;
    P0=P;
    P = subs(g,P0);
    E=abs(P-P0);
    fprintf('n=%2.0f P0=%9.8f P=%9.8f E=%e\n', cont,P0,P,E);    
end
fprintf('El valor aproximado de P es:  %9.8f\n',P)