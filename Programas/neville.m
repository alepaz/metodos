function [Q]=neville(X,Y,Xo)
n=length(X);
Q=zeros(n,n);
Q(:,1)=Y';
%Cambiar el punto siempre
for j=2:n
    for i=j:n
        Q(i,j)=((Xo-X(i-j+1))*(Q(i,j-1)) - (Xo-X(i))*(Q(i-1,j-1)))/(X(i) - X(i-j+1));
    end
end
for j=1:n
   for i=j:n
fprintf('Q(%2.0f,%2.0f)= %3.8f\n',i-1,j-1,Q(i,j))
   end
end
%Formula basica de hermite para 8(16 al doblarlas) X iniciales:
valor_aproximado=Q(n,n)
%aqui se debe de cambiar la funcion a utilizar
syms x
f=(3*x).*exp(x)-cos(x);
%f=(exp(-(x.^2)/2));
valor_exacto=subs(f,Xo)
error=abs(valor_aproximado - valor_exacto)