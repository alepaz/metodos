disp('Metodo de Horner')
error=input('ingrese el error');
X0=input('Inserte el valor X0: ');
C=input('inserte vector de coeficientes [Xo,X1, ..., Xn] : ');

% realizamos la primera división sintética para encontrar B0
    a  = metodos_division_sintetica(C, X0);
    b0 = a(end);
    a  = a(1:end - 1);
% realizamos la segunda division sintetica para encontrar Q0
    a  = metodos_division_sintetica(a, X0);
    q0 = a(end);
X=X0 - b0/q0;
E=abs(X-X0);
cont=1;
fprintf(' n=%3.0f X0=%9.8f Q0=%9.8f X=%9.8f error=%e\n', cont,X0,q0,X,E)
while E>error
    cont=cont+1;
    % realizamos la primera división sintética para encontrar B0
    a  = metodos_division_sintetica(C, X0);
    b0 = a(end);
    a  = a(1:end - 1);
% realizamos la segunda division sintetica para encontrar Q0
    a  = metodos_division_sintetica(a, X0);
    q0 = a(end);
    X=X0 - b0/q0;
    E=abs(X-X0);
    fprintf(' n=%3.0f X0=%9.8f Q0=%9.8f X=%9.8f error=%e\n', cont,X0,q0,X,E)
end
fprintf('El valor aproximado de X es:      %9.8f\n',X)