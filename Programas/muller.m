%gg funca
disp('metodo de Muller')
P0=input('introduzca el valor de P0: ');
P1=input('introduzca el valor de P1: ');
P2=input('introduzca el valor de P2: ');
syms x
g=input('introduzca la funcion: ');
error=input('introduza el valor de precision: ');
c=subs(g,P2);
fP0=subs(g,P0);
fP1=subs(g,P1);
b=((P0-P2)^2*(fP1-c) - (P1-P2)^2*(fP0-c))/((P0-P2)*(P1-P2)*(P0-P1));
a=((P1-P2)^2*(fP0-c) - (P0-P2)^2*(fP1-c))/((P0-P2)*(P1-P2)*(P0-P1));
P3=P2 - (2*c)/(b+sign(b)*sqrt(b^2 - 4*a*c));
E=abs(P3-P2);cont=1;
fprintf('n=%3.0f P0=%9.8f P1=%9.8f P2=%9.8f P3=%9.8f a=%9.8f b=%9.8f c=%9.8f error=%e\n', cont,P0,P1,P2,P3,a,b,c,E)
while E > error
    cont=cont+1;
    P0=P1;
    P1=P2;
    P2=P3;
    fP0=subs(g,P0);
    fP1=subs(g,P1);
    c=subs(g,P2);
    b=((P0-P2)^2*(fP1-c) - (P1-P2)^2*(fP0-c))/((P0-P2)*(P1-P2)*(P0-P1));
    a=((P1-P2)^2*(fP0-c) - (P0-P2)^2*(fP1-c))/((P0-P2)*(P1-P2)*(P0-P1));
    P3=P2 - (2*c)/(b+sign(b)*sqrt(b^2 - 4*a*c));
    E=abs(P3-P2);
    fprintf('n=%3.0f P0=%9.8f P1=%9.8f P2=%9.8f P3=%9.8f a=%9.8f b=%9.8f c=%9.8f error=%e\n', cont,P0,P1,P2,P3,a,b,c,E)
end
fprintf('El valor aproximado de P es:      %9.8f\n',P3)
    



