function res = metodos_division_sintetica(c, r)

% -------------------------------------------------------------------------
% c: vector de coeficientes
% r: raiz a probar
% res: vector de coeficientes del nuevo polinomio 
% -------------------------------------------------------------------------

res = c(1);

% iteramos a traves de los coeficientes
for i = 2:length(c)
    res = [res (res(end) * r + c(i))];
end