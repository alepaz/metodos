%METODO DE HORNER

grado=input('Introduzca el grado del polinomio: ');

syms x

i=1;
while i<=grado+1
   f(i)=input('valor ');
   i=i+1;
end

Xo=input('Introduzca el valor de Xo: ');
error=input('Introduzca el valor de precisión: ');

count=1;
fprintf('\nn= %3.0f\n\n',count)

i=1;
j=1;
bo(i)=f(i)*Xo+f(i+1);
i=i+1;
j=j+1;
while j<=grado
   bo(i)=bo(i-1)*Xo+f(i+1);
   i=i+1;
   j=j+1;
end

i=1;
j=1;
Qo(i)=f(i)*Xo+bo(i);
i=i+1;
j=j+1;
while j<=grado-1
   Qo(i)=Qo(i-1)*Xo+bo(i);
   i=i+1;
   j=j+1;
end

i=1;
while i<=grado+1
   j=f(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end
fprintf('\n\n            ')
i=1;
while i<=grado
   j=bo(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end
fprintf('\n\n            ')
i=1;
while i<=grado-1
   j=Qo(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end

X=Xo-bo(grado)/Qo(grado-1);
tolerancia=abs(X-Xo);
X=numeric(X);
tolerancia=numeric(tolerancia);

fprintf('\nX= %10.9f \n',X)
fprintf('|X-Xo|= %e\n',tolerancia)
fprintf('\n-------------------------------------------------------------------------\n')






while tolerancia>error
   Xo=X;
   count=count+1;
fprintf('\nn= %3.0f\n\n',count)

i=1;
j=1;
bo(i)=f(i)*Xo+f(i+1);
i=i+1;
j=j+1;
while j<=grado
   bo(i)=bo(i-1)*Xo+f(i+1);
   i=i+1;
   j=j+1;
end

i=1;
j=1;
Qo(i)=f(i)*Xo+bo(i);
i=i+1;
j=j+1;
while j<=grado-1
   Qo(i)=Qo(i-1)*Xo+bo(i);
   i=i+1;
   j=j+1;
end

i=1;
while i<=grado+1
   j=f(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end
fprintf('\n\n            ')
i=1;
while i<=grado
   j=bo(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end
fprintf('\n\n            ')
i=1;
while i<=grado-1
   j=Qo(i);
   j=numeric(j);
   fprintf(' %10.9f',j)
   i=i+1;
end

X=Xo-bo(grado)/Qo(grado-1);
tolerancia=abs(X-Xo);
X=numeric(X);
tolerancia=numeric(tolerancia);

fprintf('\nX= %10.9f \n',X)
fprintf('|X-Xo|= %e\n',tolerancia)
fprintf('\n-------------------------------------------------------------------------\n')
end
