function P = metodos_algoritmo_lagrange(XS, YS, X, PR)
%METODOS_ALGORITMO_LAGRANGE Funci�n utilizada por METODOS_LAGRANGE

% creamos las cadenas de las variables X para evaluarlas despues
varX = {};

for j = 0:(length(XS)-1)
    varX{j + 1} = sprintf('X%d', j);
end

% inicializamos la aproximacion
P = 0;

% preparamos la expresion para averigual el coeficiente Ln(x)
for a = 0:(length(XS) - 1)

    % aqui guardamos la expresion que vamos a evaluar y otras variables
    % utiles
    expresion = '';
    contador  = 0;
    
    for b = 0:(length(XS) - 1)
        
        % para que no haya division entre 0 a ~= b
        if a ~= b        
            
            % calculamos la expresion
            expresion = [expresion, sprintf('(X - X%d)/(X%d - X%d)', b, a, b)];
                         
            % aumentamos el contador
            contador = contador + 1;            
            
            % si todavia faltan terminos, entonces preparamos para
            % multiplicar
            if contador < (length(XS) - 1)
                expresion = [expresion, '*'];
            end        
        end
        
    end    
    
    % evaluamos la expresion
    evaluada = subs(expresion, varX, XS);    
    
    % mostramos lo que vamos a copiar
    fprintf(['-------------------------------------------------------', ...
             '-------------------------\n']);    
    fprintf('L%d:', a);
    pretty(sym([expresion, '=', char(evaluada)]))    
    
    % y ahora vamos evaluando para la aproximacion final
    P = P + subs(evaluada, X) * YS{a + 1};
end

fprintf(['-------------------------------------------------------', ...
         '-------------------------\n']);  
