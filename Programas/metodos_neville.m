function metodos_neville(varargin)
%Entre llaves y separados por comas las Xs {},despues las funciones
%evaluadasen llaves,despues el punto X0, despues el numero de decimales y
%despues en comillas simples las funciones
%METODOS_NEVILLE Usa el m�todo de Neville para aproximar un valor x.
%   METODOS_NEVILLE({X0, X1, ..., Xn}, {Y0, Y1, ..., Yn}, X, PR, F) utiliza los
%   valores de {X0, X1, ..., Xn} correspondientes a su valor respectivo de
%   {Y0, Y1, ..., Yn} para encontrar una aproximacion del valor X. Si se
%   proporciona la funci�n F entonces verifica el error.
%
%   METODOS_NEVILLE({X0, X1, ..., Xn}, X, PR, F) utiliza los valores de 
%   {X0, X1, ..., Xn} y los evalua en la funci�n F para aproximar un valor
%   correspondiente a X. Adem�s comprueba el error.
%
%   El resultado se muestra con la precisi�n especificada PR.
%Se emplea una matriz triangular donde se va realizando sucesivamente hasta
%que se llega al ultimo valor deseado. su formula es 
%Se debe expresar la formula y la respuesta como pocedimiento valido.!!!!!
%Qij = (X - Xi-j)Qi,j-1  -- (X - Xi)Qi-1,j-1
%      ------------------------------------------  
%                     Xi -- Xi-j
% se dan los puntos desde X0 hasta Xn y estos evaluados son los Q00 hasta
% el Qn0. Luego va el Q11 hasta el Qn1.Q22 hasta Qn2. etc
%si dan una funcion y los puntos, entonces al evaluar los puntos en la
%funcion se obtienen los primeros Q00 hasta Qn0. Mediante los dos Q
%anteriores se obtiene el proximo: Q00 y Q10 -->Q11, Q11 y Q12 -->Q22.
fprintf('\n# M�todo de Neville\n\n');

% se ha utilizado la primera forma de llamarlo
if length(varargin{2}) > 1
    
    XS = varargin{1};
    YS = varargin{2};
    X  = varargin{3};
    PR = varargin{4};
    
    if length(varargin) > 4   
        F = varargin{5};
    else
        F = [];
    end
   
% se ha utilizado la segunda forma    
else
    
    XS = varargin{1};
    X  = varargin{2};
    PR = varargin{3};
    F  = varargin{4};
    YS = num2cell(subs(F, cell2mat(XS)));
    
end

% como no hay verificaciones extras, continuamos a calcular el valor que
% queremos
fprintf('## Procedimiento:\n\n');
P = metodos_algoritmo_neville(XS, YS, X, PR);

% mostramos la soluci�n
fprintf('\n## Soluci�n:\n\n');
fprintf([' P = %.', num2str(PR), 'f\n'], P);

% si se especifico una funcion entonces podemos revisar le porcentaje de
% error
if F    
    fprintf([' F = %.', num2str(PR), 'f\n'], subs(F, X));
    fprintf(' E = %.2E\n', abs(subs(F, X) - P));    
end
    
fprintf('\n');
