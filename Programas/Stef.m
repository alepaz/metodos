disp('Steffensen')
P0=input('P0: ');
syms x
g=input('funcion: ');
error=input('precision: ');
P1=subs(g,P0);
P2=subs(g,P1);
Ps=P0 - ((P1-P2)^2)/(P2-2*P1+P0);
E=abs(Ps-P0);
cont=1;
fprintf('n=%2.0f P0=%9.8f P1=%9.8f P2=%9.8f Ps=%9.8f Error=%e\n',cont,P0,P1,P2,Ps,E)
while E>error
    cont=cont+1;
    P0=Ps;
    P1=subs(g,P0);
    P2=subs(g,P1);
    Ps=P0 - ((P1-P2)^2)/(P2-2*P1+P0);
    E=abs(Ps-P0);
    fprintf('n=%2.0f P0=%9.8f P1=%9.8f P2=%9.8f Ps=%9.8f Error=%e\n',cont,P0,P1,P2,Ps,E)
end
fprintf('Valor aproximado:  %9.8f\n',Ps)