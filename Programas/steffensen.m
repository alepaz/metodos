disp('metodo de Steffensen')
P0=input('introduzca el valor de P0: ');
syms x
g=input('introduzca la funcion: ');
error=input('introduza el valor de precision: ');
P1=subs(g,P0);
P2=subs(g,P1);
Ps=P0 - ((P1-P0)^(2))/(P2-2*P1+P0);
E = abs(Ps-P0);
cont=1;
fprintf('n=%3.0f P0=%.20f P1=%.20f P2=%.20f Ps=%.20f error=%e\n', cont,P0,P1,P2,Ps,E)
while E > error
    cont=cont+1;
    P0=Ps;
    P1=subs(g,P0);
    P2=subs(g,P1);
    Ps=P0 - ((P1-P0)^(2))/(P2-2*P1+P0);
    E = abs(Ps-P0);
    fprintf('n=%3.0f P0=%.20f P1=%.20f P2=%.20f Ps=%.20f error=%e\n', cont,P0,P1,P2,Ps,E)
end
fprintf('El valor aproximado de P es:      %9.8f\n',Ps)
    