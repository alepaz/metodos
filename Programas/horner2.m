disp('Metodo de Horner')
%Horner trabaja efectuando 2 veces la division sintetica para encontrar
%los dos valores con los q se desea para encontrar x. Siempre en la primera
%division se va a utilizar el conjunto de coeficientes originales y en la
%segunda se utilizan los coeficiente residuos de la 1 division sintetica.
%lo que cambia en cada iteracion es el X0 = X.
error=input('ingrese el error: ');
X0=input('Inserte el valor X0: ');
b0=input('Inserte el valor b0: ');
%este se obtiene mediante la primera division sintetica y el ultimo numero
%es el de b0
q0=input('Inserte el valor q0: ');
%este es el ultimo numero de la segunda division sintetica donde la base es
%el mismo X0 y se efectua con los residuos de la primera div sintetica
X=X0 - b0/q0;
E=abs(X-X0);
cont=1;
fprintf(' n=%3.0f X0=%9.8f Q0=%9.8f X=%9.8f error=%e\n', cont,X0,q0,X,E)
while E>error
    %Tu debes calcular la division sintetica
    cont=cont+1;
    X0=X;
    %el valor de x es ahora x0
    b0=input('Inserte el valor b0: ');
    %este se obtiene mediante la primera division sintetica y el ultimo numero
    %es el de b0
    q0=input('Inserte el valor q0: ');
    %este es el ultimo numero de la segunda division sintetica donde la base es
    %el mismo X0 y se efectua con los residuos de la primera div sintetica
    X=X0 - b0/q0;
    E=abs(X-X0);
    X=X0 - b0/q0;
    E=abs(X-X0);
    fprintf(' n=%3.0f X0=%9.8f Q0=%9.8f X=%9.8f error=%e\n', cont,X0,q0,X,E)
end
fprintf('El valor aproximado de X es:      %9.8f\n',X)