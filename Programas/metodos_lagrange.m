function metodos_lagrange(varargin)
%Los primeros corchetes{} son para los valores de x q da el ejercicio.
%Los segundos corchetes{} son para los valores de x evaluados en la
%funcion.
%Despues es el numero al q nos aproximamos, el valor de x q da el problema,
%al que se quiere llegar.
%Despues va el error.
%De ultimo va la funcion.
%metodos_lagrange({},{}, , ,'  ')
%METODOS_LAGRANGE Usa el m�todo de Lagrange para aproximar un valor x.
%   METODOS_LAGRANGE({X0, X1, ..., Xn}, {Y0, Y1, ..., Yn}, X, PR, F) utiliza los
%   valores de {X0, X1, ..., Xn} correspondientes a su valor respectivo de
%   {Y0, Y1, ..., Yn} para encontrar una aproximacion del valor X. Si se
%   proporciona la funci�n F entonces verifica el error.
%
%   METODOS_LAGRANGE({X0, X1, ..., Xn}, X, PR, F) utiliza los valores de 
%   {X0, X1, ..., Xn} y los evalua en la funci�n F para aproximar un valor
%   correspondiente a X. Adem�s comprueba el error.
%
%   El resultado se muestra con la precisi�n especificada PR.

fprintf('\n# M�todo de Lagrange\n\n');

% se ha utilizado la primera forma de llamarlo
if length(varargin{2}) > 1
    
    XS = varargin{1};
    YS = varargin{2};
    X  = varargin{3};
    PR = varargin{4};
    
    if length(varargin) > 4   
        F = varargin{5};
    else
        F = [];
    end
   
% se ha utilizado la segunda forma    
else
    
    XS = varargin{1};
    X  = varargin{2};
    PR = varargin{3};
    F  = varargin{4};
    YS = num2cell(subs(F, cell2mat(XS)));
    
end

% calculamos el valor deseado
fprintf('## Procedimiento:\n\n');
P = metodos_algoritmo_lagrange(XS, YS, X, PR);

% para dejar el procedimiento necesitamos saber los valores de F(X0),
% F(X1), ..., F(XN), es decir YS
fprintf('Los valores de Y son: \n');

for i = 1:length(XS)
    fprintf([' F(X%d) = %.', num2str(PR), 'f\n'], i-1, YS{i});
end

fprintf(['-------------------------------------------------------', ...
         '-------------------------\n']);  

% mostramos la soluci�n
fprintf('\n## Soluci�n:\n\n');
fprintf([' P = %.', num2str(PR), 'f\n'], P);

% si se especifico una funcion entonces podemos revisar le porcentaje de
% error
if F    
    fprintf([' F = %.', num2str(PR), 'f\n'], subs(F, X));
    fprintf(' E = %.2E\n', abs(subs(F, X) - P));    
end
    
fprintf('\n');