function metodos_hermite(varargin)
%METODOS_HERMITE Aproxima uno o varios valores de X.
%
%   METODOS_HERMITE({X0, X1, ..., Xn}, {Y0, Y1, ..., Yn}, ...
%                   {D0, D1, ..., Dn}, V, PR, F)
%
%   Utiliza los valores de {X0, X1, ..., Xn} correspondientes a su valor 
%   respectivo de {Y0, Y1, ..., Yn}, en donde {D0, D1, ..., Dn} son los valores
%   de X evaluada en la primera derivada de la funci�n. Con esto se encuentra
%   una aproximacion de el valor V. Si se proporciona la funci�n F entonces se
%   verifica el error.
%
%   METODOS_HERMITE({X0, X1, ..., Xn}, V, PR, F)
%
%   Utiliza los valores de {X0, X1, ..., Xn} y los evalua en la funci�n F
%   para aproximar el valor V. Adem�s comprueba el error.
%
%   El resultado se muestra con la precisi�n especificada PR.

fprintf('\n# M�todo de hermite\n\n');

% -------------------------------------------------------------------------
% PREPARACI�N
% -------------------------------------------------------------------------

% chequeo de errores
if ~iscell(varargin{1})
    error('El primer argumento debe de ser un arreglo de celdas.');
end

% se ha utilizado la primera forma de llamarlo
if iscell(varargin{1}) && iscell(varargin{2}) && iscell(varargin{3})
    
    XS = varargin{1};
    YS = varargin{2};
    DS = varargin{3};
    X  = varargin{4};
    PR = varargin{5};
    
    if length(varargin) > 5
        FU = varargin{6};
    else
        FU = [];
    end
   
% se ha utilizado la segunda forma    
else
    
    XS = varargin{1};
    X  = varargin{2};
    PR = varargin{3};
    FU = varargin{4};
    YS = num2cell(subs(FU, cell2mat(XS)));  
    DS = num2cell(subs(diff(FU), cell2mat(XS)));  
    
end

% los datos deben de ser coherentes
if length(XS) ~= length(YS)
    error('No se han proporcionado la misma cantidad de valores de X y Y');
end

fprintf('## Preparaci�n:\n\n');

for i = 1:length(XS)
    fprintf(['  f(%g) = %.', num2str(PR), 'f\n'], ...
             XS{i}, YS{i});
end

fprintf('\n');

% duplicamos los valores de XS, YS y DS porque asi se hace en este m�todo
XS_ = XS;
YS_ = YS;
DS_ = DS;
XS  = {};
YS  = {};
DS  = {};
for i = 1:length(XS_)
    XS{2*(i - 1) + 1} = XS_{i};
    XS{2*(i - 1) + 2} = XS_{i};
    
    YS{2*(i - 1) + 1} = YS_{i};
    YS{2*(i - 1) + 2} = YS_{i};
    
    DS{2*(i - 1) + 1} = DS_{i};
    DS{2*(i - 1) + 2} = DS_{i};
end

% aqui guardamos la aproximaci�n
P = 0;

% -------------------------------------------------------------------------
% ALGORITMO
% -------------------------------------------------------------------------

fprintf('## Procedimiento:\n\n');

% creamos el arreglo de celda vacio F
F = num2cell(zeros(length(XS)));

% ponemos los valores de la primera columna
F(:, 1) = YS;

% creamos las cadenas de las variables F para evaluarlas despues
varF = {};

for j = 0:(length(XS)-1)
    for i = 0:(length(XS)-1)
        varF{i + 1, j + 1} = sprintf('F%d_%d', i, j);        
    end    
end

% ahora la de las variables X
varX = {};

for j = 0:(length(XS)-1)
    varX{j + 1} = sprintf('X%d', j);
end

% en la variable vamos a ir guardando la expresi�n (x - x0)(x - x1)...
% (x - xn) y tambi�n guardamos el polinomio final
multiplic = '';
polinomio = 'FF0_0';
P = F{1, 1};

% iteramos para crear una matriz tiangular inferior
for j = 1:(length(XS)-1)  
    
    fprintf(['=======================================================', ...
             '=========================\n']);   
    fprintf(' COLUMNA %d\n', j);
    fprintf(['=======================================================', ...
             '=========================\n\n']);   
    
    for i = j:(length(XS)-1)
       
        % si se indeterminar�a, entonces lo que hacemos es evaluarla en su
        % derivada
        if XS{i+1} == XS{i-j+1}
           
            expresion = 'DF(x)';
            F{i + 1, j + 1} = DS{i+1};
            
        % si no hacemos lo mismo que en el de diferencias divididas
        else                    
            expresion = sprintf('(F%d_%d - F%d_%d)/(X%d - X%d)', i, j-1, i-1, ...
                                j-1, i, i-j);

            % primero evaluamos con las F
            evaluada = subs(expresion, varF(:), F(:));

            % despues con los x0, x1, ..., xn
            evaluada = subs(evaluada, varX(:), XS(:));

            % ultimamente con la x que deseamos aproximar
            F{i + 1, j + 1} = subs(evaluada, X);
        end
        
        % mostramos la igualdad para copiarla al parcial :)
        fprintf(['-------------------------------------------------------', ...
                 '-------------------------\n']);     
        fprintf('F%d_%d: (%g)', i, j, F{i + 1, j + 1});
        valor = sprintf(['%.', num2str(PR), 'f'], F{i + 1, j + 1});        
        pretty(sym([expresion, '=', valor]))
        
        % si se trata del primer valor en la columna entonces expandemos el
        % polinomio final                
        if j == i

            % le agregamos el coeficiente            
            polinomio = [polinomio, ...
                         sprintf(['+(F%d_%d)'], i, j)];                     
                     
            % agregamos un factor mas a la multiplicaci�n
            multiplic = [multiplic, '*(X - (', num2str(XS{j}), '))'];
                     
            % agregamos las multiplicaiones
            polinomio = [polinomio, multiplic];
            
            % agregamos a la aproximaci�n este termino
            P = P + F{i + 1, j + 1} * subs(['1', multiplic], X);

        end             
    end
    
    fprintf(['-------------------------------------------------------', ...
                 '-------------------------\n\n']);    
             
end

% -------------------------------------------------------------------------
% RESULTADOS
% -------------------------------------------------------------------------
fprintf('## Polinomio sin formato:\n\n');
fprintf(['  ', polinomio, '\n\n']);
fprintf('## Polinomio con formato y simplificado:\n');
pretty(sym(polinomio));
fprintf('\n');

fprintf('## Resultado para X = %g:\n\n', X);
fprintf(['  Aproximado: P(%g) = %.', num2str(PR), 'f\n'], X, P);
        
% si se tiene la funcion se calcula el valor exacto y el error
if FU
    fprintf(['      Exacto: F(%g) = %.', num2str(PR), 'f\n'], X, ...
             subs(FU, X));
    fprintf('       Error: %.2E\n', ...
            abs(subs(FU, X) - P));
end
%Se realiza igual que diferencias divididas, con la diferencia q cada punto
%se pone 2 veces. La primera fila se calcula evaluando el punto en la
%funcion. Cuando en la segunda fila se evalua un valor q contiene a los
%valores anteriores iguales, se evalua con la derivada de la funcion.Luego
%el valor de cada fila se obtiene normalmente como diferencias divididas.
%Formula: Pn(x)=F[X0]+F[X0,X1](X-X0)+F[X0,X1,X2](X-X0)(X-X1)+...

