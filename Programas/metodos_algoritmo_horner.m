function P = metodos_algoritmo_horner(C, T, X0, PR)
%C=vector de coeficientes
%X0=raiz a probar
%T=tolerancia o precision
% METODOS_ALGORITMO_HORNER Funci�n utilizada por METODOS_HORNER

% se especifican un numero maximo de 1000 iteraciones, si pasan las 1000 y
% no se ha encontrado una respuesta posiblemente algo esta mal
for it = 1:1000
    
    % realizamos la primera divisi�n sint�tica para encontrar B0
    a  = metodos_division_sintetica(C, X0);
    b0 = a(end);
    a  = a(1:end - 1);
    
    % realizamos la segunda division sintetica para encontrar Q0
    a  = metodos_division_sintetica(a, X0);
    q0 = a(end);
    
    % ahora se puede calcular el valor de x correspondiente
    x = subs('x0 - b0/q0', {'x0', 'b0', 'q0'}, {X0, b0, q0});
    
    % calculamos el error
    E = abs(x - X0);
    
    % mostramos la informaci�n en pantalla
    fprintf(['-------------------------------------------------------', ...
             '-------------------------\n']);  
    fprintf(' % 2d. X%d:', it, it);
    pretty(sym(sprintf(['X%d - B%d/Q%d = %.', PR, 'f'], it-1, it-1, it-1, x)));
    fprintf('     Error = %.2E\n', E);
    
    % si ya tenemos la precisi�n requerida, entonces terminamos
    if E < T
        break
    end    
    
    % preparamos los valores para la siguiente iteracion
    X0 = x;    
    
end

fprintf(['-------------------------------------------------------', ...
         '-------------------------\n']);  

% si llegamos a la iteraci�n n�mero 1000, lo m�s probable es que no hayamos
% encontrado un valor
if it == 1000
    error('Se realizaron 999 iteraciones y no se encontr� un resultado.');
end