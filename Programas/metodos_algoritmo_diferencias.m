function PO = metodos_algoritmo_diferencias(XS, YS, X, PR)
%METODOS_ALGORITMO_DIFERENCIAS Funci�n utilizada por METODOS_DIFERENCIA

% creamos el arreglo de celda vacio F
F = num2cell(zeros(length(XS)));

% ponemos los valores de la primera columna
F(:, 1) = YS;

% creamos las cadenas de las variables F para evaluarlas despues
varF = {};

for j = 0:(length(XS)-1)
    for i = 0:(length(XS)-1)
        varF{i + 1, j + 1} = sprintf('F%d%d', i, j);        
    end    
end

% ahora la de las variables X
varX = {};

for j = 0:(length(XS)-1)
    varX{j + 1} = sprintf('X%d', j);
end

% inicializamos la aproximacion y un contador que vamos a ocupar
P = 0;

% en la variable vamos a ir guardando la expresi�n (x - x0)(x - x1)...
% (x - xn) y tambi�n guardamos el polinomio final
multiplic = '';
polinomio = sprintf(['+(%.', num2str(PR), 'f)'], F{1, 1});

% iteramos para crear una matriz tiangular inferior
for j = 1:(length(XS)-1)  
    for i = j:(length(XS)-1)
       
        expresion = sprintf('(F%d%d - F%d%d)/(X%d - X%d)', i, j-1, i-1, j-1, ...
                            i, i-j);
                                
        % primero evaluamos con las F
        evaluada = subs(expresion, varF(:), F(:));
        
        % despues con los x0, x1, ..., xn
        evaluada = subs(evaluada, varX(:), XS(:));
                    
        % ultimamente con la x que deseamos aproximar
        F{i + 1, j + 1} = subs(evaluada, X);
        
        % mostramos la igualdad para copiarla al parcial :)
        fprintf(['-------------------------------------------------------', ...
                 '-------------------------']);        
        valor = sprintf(['%.', num2str(PR), 'f'], F{i + 1, j + 1});        
        pretty(sym([expresion, '=', valor]))
        
        % si se trata del primer valor en la columna entonces expandemos el
        % polinomio final                
        if j == i

            % le agregamos el coeficiente            
            polinomio = [polinomio, ...
                         sprintf(['+(%.', num2str(PR), 'f)'], F{i + 1, j + 1})];                     
                     
            % agregamos un factor mas a la multiplicaci�n si no se trata
            % del �ltimo
            if i ~= (length(XS) - 1)
                multiplic = [multiplic, '*(X - (', num2str(XS{j}), '))'];
            end                  
                     
            % agregamos las multiplicaiones
            polinomio = [polinomio, multiplic];

        end             
    end
end

% mostramos el polinomio final
PO = polinomio;
