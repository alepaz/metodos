%Limpiar todos las variables en memoria
clear all
%Limpia el texto en matlab
clc
disp('Metodo de la biseccion')
a=input('introduzca el valor de a: ');
b=input('introduzca el valor de b: ');
syms x
g=input('introduzca la funcion: ');
error=input('introduza el valor de precision: ');
fa=subs(g,a);
fb=subs(g,b);
if fa*fb < 0
    c=a+(b-a)/2;
    fc=subs(g,c);
    cont=1;
    tol=abs(fc);
    fprintf(' n=%3.0f a=%9.8f b=%9.8f c=%9.8f tol=%e\n', cont,a,b,c,tol)
    while abs(c-a) > error
        cont = cont+1;
        if fa*fc < 0
            a=a;
            b=c;
            c=a+(b-a)/2;
            tol=abs(c-b);
        else
            a=c;
            b=b;
            c=a+(b-a)/2;
            tol=abs(c-a);
        end
        fc=subs(g,c);
        fprintf(' n=%3.0f a=%9.8f b=%9.8f c=%9.8f tol=%e\n', cont,a,b,c,tol)
    end
end
fprintf('El valor aproximado de x es:      %9.8f\n',c)
%Trazadores Cubicos
%Se da una tabla en donde se elijen los intervalos entre cada punto, h es
%el espaciado es h y es cada espaciado entre punto y punto de los
%intervalos si hay n puntos, hay n-1 intervalos, si ya dan los valores de
%la funcion evaluada, esos son los valores de A.
%Las funciones son F[xj]=Aj;    dj= Cj+1 - Cj / 3hj
%bj = Aj+1 - Aj / hj -- hj(2Cj + Cj+1)/3
%hj-1Cj-1 + 2(hj- + hj)Cj + hjCj+1 = 3(Aj+1 - Aj)/hj -- 3(Aj - Aj-1)/hj-1
