disp('metodo de posicion falsa')
P0=input('introduzca el valor de P0: ');
P1=input('introduzca el valor de P1: ');
syms x
g=input('introduzca la funcion: ');
error=input('introduza el valor de precision: ');
fP0=subs(g,P0);
fP1=subs(g,P1);
if fP0*fP1 < 0
    P2 = P1 - ((P1 - P0)*fP1)/(fP1 - fP0);
    cont=1;
    E = abs(P2 - P1);
    fprintf('n=%3.0f P0=%9.10f P1=%9.10f P2=%9.10f error=%e\n', cont,P0,P1,P2,E)
    while E > error
        fP0=subs(g,P0);
        fP1=subs(g,P1);
        fP2=subs(g,P2);
        if fP1*fP2 < 0
            P0=P1;
            P1=P2;
            P2 = P1 - ((P1 - P0)*fP1)/(fP1 - fP0);
            cont=cont+1;
            E = abs(P2 - P1);
            fprintf('n=%3.0f P0=%9.10f P1=%9.10f P2=%9.10f error=%e\n', cont,P0,P1,P2,E)
        else
            P0=P0;
            P1=P2;
            P2 = P1 - ((P1 - P0)*fP1)/(fP1 - fP0);
            cont=cont+1;
            E = abs(P2 - P1);
            fprintf('n=%3.0f P0=%9.10f P1=%9.10f P2=%9.10f error=%e\n', cont,P0,P1,P2,E)
        end
    end
end
%fprintf('El valor aproximado de P es:      %9.10f\n',P2)
    
    