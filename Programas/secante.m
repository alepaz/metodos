disp('Metodo de la Secante')
P0=input('introduzca el valor de P0: ');
P1=input('introduzca el valor de P1: ');
syms x
g=input('introduzca la funcion: ');
error=input('introduza el valor de precision: ');
fP0=subs(g,P0);
fP1=subs(g,P1);
P2 = P1 - ((P1 - P0)*fP1)/(fP1 - fP0);
cont=1;
E = abs(P2 - P1);
fprintf(' n=%3.0f P0=%.15f P1=%.15f P2=%.15f error=%e\n', cont,P0,P1,P2,E)
while E>error
    cont=cont+1;
    P0=P1;
    P1=P2;
    fP0=subs(g,P0);
    fP1=subs(g,P1);
    P2 = P1 - ((P1 - P0)*fP1)/(fP1 - fP0);
    E = abs(P2 - P1);
    fprintf(' n=%3.0f P0=%.15f P1=%.15f P2=%.15f error=%e\n', cont,P0,P1,P2,E)
end
fprintf('El valor aproximado de P es:      %.15f\n',P2)
