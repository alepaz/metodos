disp('Secante')
P0=input('P0: ');
P1=input('P1: ');
error=input('precision: ');
syms x
g=input('funcion: ');
fP0=subs(g,P0);
fP1=subs(g,P1);
P2=P1 - ((P1-P0)*fP1)/(fP1-fP0);
E=abs(P2-P1);
cont=1;
fprintf('P0=%9.8f P1=%9.8f P2=%9.8f Error=%e\n',P0,P1,P2,E)
while E>error
    cont=cont+1;
    P0=P1;
    P1=P2;
    fP0=subs(g,P0);
    fP1=subs(g,P1);
    P2=P1 - ((P1-P0)*fP1)/(fP1-fP0);
    E=abs(P2-P1);
    fprintf('P0=%9.8f P1=%9.8f P2=%9.8f Error=%e\n',P0,P1,P2,E)
end
fprintf('Valor de P:  %9.8f',P2)