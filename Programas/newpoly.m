function [D]=newpoly(X,Y,Xo)
%Para trabajar con esta funcion, debo de cambiar la funcion hasta abajo y
%el punto a evaluar, tambien debo de iniciarla primero dandole valores a
%"x" luego a "y" y despues con "[D]=newpoly(x,y)"
%Lo q se necesitan son los valores de X0 hasta Xn de acuerdo al numero de
%puntos dados, luego se evaluan en la funcion para obtener la fila de
%F[Xi], con esta fila se obtiene la fila F[Xi,Xi+1]y asi F[Xi,Xi+1,Xi+2]...se calcula 
%F[Xi,Xi+1] = F[Xi+1]-F[Xi] / Xi+1 - Xi, y asi sucesivamente 
%F[Xi,Xi+1,Xi+2] = F[Xi+1,Xi+2]-F[Xi,Xi+1] / Xi+2 - Xi
n=length(X);
D=zeros(n,n);
D(:,1)=Y';
for j=2:n
    for k=j:n
        D(k,j)=(D(k,j-1)-D(k-1,j-1))/(X(k)-X(k-j+1));
    end
end
%Formula basica de dif div:
syms x
%Cambiar D po F[] el numero indica hasta cual X se llega.
P=D(1,1)+D(2,2)*(x-X(1))+D(3,3)*(x-X(1))*(x-X(2))+D(4,4)*(x-X(1))*(x-X(2))*(x-X(3))+...
  D(5,5)*(x-X(1))*(x-X(2))*(x-X(3))*(x-X(4))%+D(6,6)*(x-X(1))*(x-X(2))*(x-X(3))*(x-X(4))*(x-X(5))+...
  %D(7,7)*(x-X(1))*(x-X(2))*(x-X(3))*(x-X(4))*(x-X(5))*(x-X(6))
  %D(8,8)*(x-X(1))*(x-X(2))*(x-X(3))*(x-X(4))*(x-X(5))*(x-X(6))*(x-X(7))
% el punto a evaluar se debe de cambiar por 3.75
valor_aproximado=subs(P,x,Xo)
%aqui se debe de cambiar la funcion a utilizar
%f=log(2-x);
%valor_exacto=subs(f,Xo)
%error=abs(valor_aproximado - valor_exacto)

    